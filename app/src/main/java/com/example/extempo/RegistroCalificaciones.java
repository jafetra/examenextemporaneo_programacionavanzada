package com.example.extempo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class RegistroCalificaciones extends AppCompatActivity {

    private String grupo;
    private Spinner spnMaterias;
    private EditText txtMatricula;
    private EditText txCalificacion;
    private Button btnGuardar;
    private Button btnLista;
    private Button btnSalir;
    private DBCalificaciones db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registro);
        db = new DBCalificaciones(this);
        grupo = getIntent().getStringExtra("grupo");


        initView();
    }

    private void initView() {
        spnMaterias = (Spinner) findViewById(R.id.spnMaterias);
        txtMatricula = (EditText) findViewById(R.id.txtMatricula);
        txCalificacion = (EditText) findViewById(R.id.txCalificacion);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnLista = (Button) findViewById(R.id.btnLista);
        btnSalir = (Button) findViewById(R.id.btnSalir);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtMatricula.getText().toString().matches("") || txCalificacion.getText().toString().matches(""))
                {
                    Toast.makeText(RegistroCalificaciones.this, "No puedes dejar los campos vacios", Toast.LENGTH_LONG).show();
                    return;
                }


                Calficacion calficacion = new Calficacion();
                calficacion.setCalficiacion(Float.parseFloat(txCalificacion.getText().toString()));
                calficacion.setMatircula(txtMatricula.getText().toString());
                calficacion.setGrupo(grupo);
                calficacion.setMateria(spnMaterias.getSelectedItem().toString());
                db.insertCalificacion(calficacion);
                txtMatricula.setText("");
                txCalificacion.setText("");
            }
        });

        findViewById(R.id.btnSalir).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.btnLista).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegistroCalificaciones.this, ListaCalficiaciones.class));
            }
        });
    }
}
